/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista;

/**
 *
 * @author abiga
 */
public class Estudiante {
         private String nombre; 
    private String ap_P;
    private String ap_M;
    private String carrera;
    private boolean inscrito;
    
    
    
   public Estudiante(String nombre, String ap_P, String ap_M, String carrera, boolean inscrito) {  
        this.nombre = nombre;
        this.ap_P = ap_P;
        this.ap_M = ap_M;
        this.carrera = carrera;
        this.inscrito = inscrito;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAp_P() {
        return ap_P;
    }

    public void setAp_P(String ap_P) {
        this.ap_P = ap_P;
    }

    public String getAp_M() {
        return ap_M;
    }

    public void setAp_M(String ap_M) {
        this.ap_M = ap_M;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public boolean isInscrito() {
        return inscrito;
    }

    public void setInscrito(boolean inscrito) {
        this.inscrito = inscrito;

    
}
}
