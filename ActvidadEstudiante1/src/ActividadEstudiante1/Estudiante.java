/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ActividadEstudiante1;

/**
 *
 * @author Home
 */
public class Estudiante {
    private final String nombre;
    private final String ApellidoPaterno;
    private final String ApellidoMaterno;
    private final String Carrera; 
    private final boolean Inscrito; 
    boolean getNombre;

    public Estudiante(String nombre, String ApellidoPaterno, String ApellidoMaterno, String Carrera, boolean Inscrito) {
        this.nombre = nombre;
        this.ApellidoPaterno = ApellidoPaterno;
        this.ApellidoMaterno = ApellidoMaterno;
        this.Carrera = Carrera;
        this.Inscrito = Inscrito;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public String getCarrera() {
        return Carrera;
    }

    public boolean isInscrito() {
        return Inscrito;
    }
    
    
    
}
