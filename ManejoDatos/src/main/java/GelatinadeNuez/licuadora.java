/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GelatinadeNuez;

/**
 *
 * @author Axel
 */
public class licuadora { 
   //Una clase esta compuesta por: nombre 
   //atributos: Son las cualidades del objeto
   // método: Las acciones que hace la clase
 //Atributos de una licuadora
     private String marca; 
      private String modelo;
      private String tamaño;
      private String color;
  //¿Qué hace una licuadora? 
     public licuadora(){
         this.marca = "t-fal";
     }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
     
     public void licuar(){
         System.out.println("Estoy licuando");
     }
     public void triturar(){
         System.out.println("");
     }
     public void apagar(){
         System.out.println("");
     }              
    public String mezclar (String ingredientes){
        return("ingredientes licuados");
    }
}
