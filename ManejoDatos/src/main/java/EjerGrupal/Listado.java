/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EjerGrupal;

/**
 *
 * @author Axel
 */
public class Listado {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       // Estudiante est = new Estudiante ("Axel","Cornejo","Peralta", "Actuaria",true);
       Estudiante est1 = new Estudiante("Victor Hugo","Mendoza", "Vences","Actuaria", true);
       Estudiante est2 = new Estudiante("Katia Denisse", "Martinez", "Engallo","Actuaria", true);
       Estudiante est3 = new Estudiante("Karime", "Solís", "Mendoza","Desconocido", true);
       Estudiante est4 = new Estudiante("Luis Axel","Cornejo","Peralta", "Actuaria",true);
       Estudiante est5 = new Estudiante("Iris Beatriz", "García", "Peralta","Desconocido", true);
       Estudiante est6 = new Estudiante("Lesly Abigail","Delgado","Medina","Actuaria", true);  //Lista[6]=
       Estudiante est7 = new Estudiante("Ruth", "González","Ramirez","Actuaria", true);
       Estudiante est8 = new Estudiante("Guillermo","Aguirre","Armada","Desconocido", true);
       Estudiante est9 = new Estudiante("Ruth Itzel","García","Cadena","Actuaria", true);
       Estudiante est10 = new Estudiante("Min Gyu","Cho", " ","Actuaria", true);
       Estudiante est11= new Estudiante("Aurora","Dominguez","Amaro","Actuaria", true);
       Estudiante est12 = new Estudiante("José Alfredo","Sequera","Fernández","Desconocido", true);
       Estudiante est13 = new Estudiante("Karla Yoloxochitl","Erazo","Amaya","Actuaria", true);
       Estudiante est14 = new Estudiante("Juan","Wong","Mestas","Actuaria", true);
       Estudiante est15 = new Estudiante("Ximena","Juárez","Salazar","Matemáticas aplicadas", true);
       Estudiante est16 = new Estudiante("Vanessa","Gómez","Nicolas","Desconocido", true);
       Estudiante est17 = new Estudiante("José Ángel","Soto","Reyes","Matemáticas aplicadas", true);
       Estudiante est18 = new Estudiante("Marcelo","Bartolucci", "Nicolás","Desconocido", true);
       Estudiante est19 = new Estudiante("Laura Fernanda","Mendoza","García","Desconocido", true);
       Estudiante est20 = new Estudiante("Caterinne Estefanny","Bolaños","López","Desconocido", true);
      Estudiante est21 = new Estudiante("Alejandra","Lopez","Herrera","Actuaria", true);
   //    Estudiante est1 = new Estudiante("", "", "","Actuaria", true);
       Estudiante[] Lista= new Estudiante[22];
       Lista[0]= est1;
       Lista[1]= est2;
       Lista[2]= est3;
       Lista[3]= est4;
       Lista[4]= est5;
       Lista[5]= est6;
       Lista[6]= est7;
       Lista[7]= est8;
       Lista[8]= est9;
       Lista[9]= est10;
       Lista[10]= est11;
       Lista[11]= est12;
       Lista[12]= est13;
       Lista[13]= est14;
       Lista[14]= est15;
       Lista[15]= est16;
       Lista[16]= est17;
       Lista[17]= est18;
       Lista[18]= est19;
       Lista[19]= est20;
       Lista[20]= est21;
       
       //Ejercicio 1 se muestran todos los alumnos del curso
//       for(int i=0; i<Lista.length; i++)
//       { 
//           System.out.println(Lista[i].getNombre()+ Lista[i].getApellidoPaterno()+ Lista[i].getApellidoMaterno()+ Lista[i].getCarrera());
//       }
       
       //Ejercicio 2 Se muestran solo los alumnos  que no son de actuaría
//        for (int i = 0; i < Lista.length; i++) {
//            if (Lista[i].getCarrera()!="Actuaria") {
//                System.out.println(Lista[i].getNombre()+ Lista[i].getApellidoPaterno()+ Lista[i].getApellidoMaterno()+ Lista[i].getCarrera());
//            }
//        }
     //Ejercicio 3
//     Lista[2] = null;
//        for (int i = 0; i < Lista.length; i++) {
//            if (i!=2) {
//                System.out.println(Lista[i].getNombre()+ Lista[i].getApellidoPaterno()+ Lista[i].getApellidoMaterno()+ Lista[i].getCarrera());
//            }
//        }
//      
        //Ejercicio 4
        Estudiante est22  = new Estudiante("Alan","Moreno","Rosas","Computación",true);
        Lista[21]=est22;
        for (int i = 0; i < Lista.length; i++) {
            System.out.println(Lista[i].getNombre()+ Lista[i].getApellidoPaterno()+ Lista[i].getApellidoMaterno()+ Lista[i].getCarrera());
        }
    }
    
}
