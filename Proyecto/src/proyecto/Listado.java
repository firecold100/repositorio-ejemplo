/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

/**
 *
 * @author Hp
 */
public class Listado {
    public static void main(String[] args) {
        //Estudiante est = new Estudiante("José","Soto","Reyes","Matematicas Aplicadas",false);
        Estudiante[] Lista = new Estudiante[22];

        Lista[0] = new Estudiante("Victor Hugo", "Mendoza", "Vences", "Actuaria", true);
        Lista[1] = new Estudiante("Katia Denisse", "Martinez", "Engallo", "Actuaria", true);
        Lista[2] = new Estudiante("Karime", "Solis", "Mendoza", "No se sabe", false);
        Lista[3] = new Estudiante("Luis Axel", "Cornejo", "Peralta", "Actuaria", true);
        Lista[4] = new Estudiante("Iris Beatriz", "Garcia", "Peralta", "No se sabe", false);
        Lista[5] = new Estudiante("Lesly Abigail", "Delgado", "Medina", "Actuaria", true);
        Lista[6] = new Estudiante("Ruth", "Gonzales", "Ramirez", "Actuaria", true);
        Lista[7] = new Estudiante("Guillermo", "Aguirre", "Armada", "Actuaria", true);
        Lista[8] = new Estudiante("Ruth Itzel", "Garcia", "Cadena", "Actuaria", true);
        Lista[9] = new Estudiante("Min Gyu", "Cho", "Sin Ape", "Actuaria", true);
        Lista[10] = new Estudiante("Aurora", "Dominguez", "Amaro", "Actuaria", true);
        Lista[11] = new Estudiante("José Alfredo", "Sequera", "Fernandez", "No se sabe", false);
        Lista[12] = new Estudiante("Karla Yoloxochitl", "Erazo", "Amaya", "Actuaria", true);
        Lista[13] = new Estudiante("Juan", "Wong", "Mestas", "Actuaria", true);
        Lista[14] = new Estudiante("Ximena", "Juarez", "Salazar", "Matematicas Aplicadas", false);
        Lista[15] = new Estudiante("Vanessa", "Gomez", "Nicolas", "No se sabe", false);
        Lista[16] = new Estudiante("Jose Angel", "Soto", "Reyes", "Matematicas Aplicadas", false);
        Lista[17] = new Estudiante("Marcelo", "Bartolucci", "Nicolas", "No se sabe", false);
        Lista[18] = new Estudiante("Laura Fernanda", "Mendoza", "Garcia", "No se sabe", false);
        Lista[19] = new Estudiante("Caterinne Estefanny", "Bolaños", "Lopez", "No se sabe", false);
        Lista[20] = new Estudiante("Alejandra", "Lopez", "Herrera", "Actuaria", true);

        for (Estudiante Lista1 : Lista) {
            //Mostrar alumnos del curso
            System.out.println(Lista1.getNombre() + Lista1.getApellidoPaterno() + Lista1.getApellidoMaterno() + Lista1.getCarrera());
        }

        for (Estudiante Lista1 : Lista) {
            //Desplegar a los alumnos de actuaria
            if (!"Actuaria".equals(Lista1.getCarrera())) {
                System.out.println(Lista1.getNombre() + Lista1.getApellidoPaterno() + Lista1.getApellidoMaterno() + Lista1.getCarrera());
            }
            Lista1.getNombre();
            System.out.println(Lista1.getNombre);
        }

        Lista[2] = null; //Eliminar el 3er elemento del arreglo

        for (int i = 0; i < Lista.length; i++) {
            if (i != 2) {

                System.out.println(Lista[i]);
            }

            Lista[21] = new Estudiante("Alan", "Moreno", "Rosas", "Computacion", true);// Agregar un estudiante al arreglo
            System.out.println(Lista[i].getNombre() + Lista[i].getApellidoPaterno() + Lista[i].getApellidoMaterno() + Lista[i].getCarrera());

        }
    }

}
